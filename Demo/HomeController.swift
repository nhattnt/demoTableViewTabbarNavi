//
//  SecondViewController.swift
//  Demo
//
//  Created by nhatnt on 05/10/2017.
//  Copyright © 2017 nhatnt. All rights reserved.
//

import UIKit

class HomeController: UIViewController {

    @IBAction func showHomeDetail(_ sender: Any) {
        performSegue(withIdentifier: "showDetail", sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
