//
//  BaseCell.swift
//  Demo
//
//  Created by nhatnt on 06/10/2017.
//  Copyright © 2017 nhatnt. All rights reserved.
//

import UIKit

class BaseCell: UITableViewCell {

    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    var item: ProfileViewModelItem? 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
