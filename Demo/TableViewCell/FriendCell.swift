//
//  FriendCell.swift
//  Demo
//
//  Created by nhatnt on 04/10/2017.
//  Copyright © 2017 nhatnt. All rights reserved.
//

import UIKit

class FriendCell: UITableViewCell {
    //MARK: UI Elements
    @IBOutlet weak var friendImage: UIImageView!
    @IBOutlet weak var friendName: UILabel!

    //MARK: Variables
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    
    var item: Friend? {
        didSet {
            guard let item = item else {
                return
            }
            
            if let pictureUrl = item.pictureUrl {
                friendImage?.image = UIImage(named: pictureUrl)
            }
            
            friendName?.text = item.name
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        friendImage?.layer.cornerRadius = 27
        friendImage?.clipsToBounds = true
        friendImage?.contentMode = .scaleAspectFit
        friendImage?.backgroundColor = UIColor.blue
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
