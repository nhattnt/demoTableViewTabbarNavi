//
//  ViewController.swift
//  Demo
//
//  Created by nhatnt on 04/10/2017.
//  Copyright © 2017 nhatnt. All rights reserved.
//

import UIKit

class ProfileController: UIViewController, UITableViewDelegate, UINavigationControllerDelegate{
    let viewModel = ProfileViewModel()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView?.dataSource = viewModel
        tableView?.delegate = viewModel
        
        tableView?.register(AboutCell.nib, forCellReuseIdentifier: AboutCell.identifier)
        tableView?.register(NameAndPictureCell.nib, forCellReuseIdentifier: NameAndPictureCell.identifier)
        tableView?.register(FriendCell.nib, forCellReuseIdentifier: FriendCell.identifier)
        tableView?.register(AttributeCell.nib, forCellReuseIdentifier: AttributeCell.identifier)
        tableView?.register(EmailCell.nib, forCellReuseIdentifier: EmailCell.identifier)
    }
}

